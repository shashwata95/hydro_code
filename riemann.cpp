#include <iostream>
#include <cmath>
#include "all_constants.h"
#include "riemann.h"

using namespace std;

void RiemannLF::ComputeFlux(Cell cL, Cell cR, double *flux, double vmax){
	double fL[nvar], fR[nvar];
	double W1[nvar], W2[nvar];
	for (int var=0; var<nvar; var++) W1[var] = 0.5*(cL.WL[var] + cL.WR[var]);
	for (int var=0; var<nvar; var++) W2[var] = 0.5*(cR.WL[var] + cR.WR[var]);


	CellCentredFlux(W1, fL);
	CellCentredFlux(W2, fR);
	for (int var=0; var<nvar; var++) 
		flux[var] = 0.5*(fL[var] + fR[var]) + 0.5*vmax*(cL.qR[var] - cR.qL[var]);
	// std::cout<<"flux: ";
	// for (int var=0; var<nvar; var++)
	// 	std::cout<<flux[var]<<"	";
	// std::cout<<endl;

	return;
}

void RiemannLF::ComputeFlux(double *qL, double *qR, double *flux, double vmax){
	double fL[nvar], fR[nvar];
	double qavg[nvar];
	for (int var=0; var<nvar; var++) qavg[var] = 0.5*(qL[var] + qR[var]);
	CellCentredFlux(qavg, fL);
	CellCentredFlux(qavg, fR);
	for (int var=0; var<nvar; var++) 
		flux[var] = 0.5*(fL[var] + fR[var]) + 0.5*vmax*(qL[var] - qR[var]);

	return;
}


void RiemannLF::CellCentredFlux(Cell cell, double *flux){
	double vx = cell.vel[0];
	flux[irho] = cell.rho*vx; //density flux
	for (int jj=0; jj<ndim; jj++) flux[jj] = cell.rho*vx*cell.vel[jj]; //velocity flux
	flux[0] += cell.press;
	flux[iE] = gamma1/(gamma1-1.)*cell.press;
	for (int jj=0; jj<ndim; jj++) flux[iE] += 0.5*cell.rho*cell.vel[jj]*cell.vel[jj];
	flux[iE] *= vx;

	return;
}

void RiemannLF::CellCentredFlux(double * W, double *flux){
	double vx = W[0];
	flux[irho] = W[irho]*vx; //density flux
	for (int jj=0; jj<ndim; jj++) flux[jj] = W[irho]*vx*W[jj]; //velocity flux
	flux[0] += W[iE];
	flux[iE] = gamma1/(gamma1-1.)*W[iE];
	for (int jj=0; jj<ndim; jj++) flux[iE] += 0.5*W[irho]*W[jj]*W[jj];
	flux[iE] *= vx;

	return;
}


// void RiemannLF::CellCentredFlux(double* q, double *flux){
// 	// double qavg[nvar];
// 	double press, ekin = 0.;

// 	// for (int var=0; var<nvar; var++) qavg[var] = 0.5*(qL[var] + qR[var]);


// 	for (int kk=0; kk<ndim; kk++) ekin += 0.5*q[kk]*q[kk]/q[irho];
// 	press = (gamma1-1.)*(q[iE] - ekin);

// 	double vx = q[0]/q[irho];
// 	flux[irho] = q[0]; //density flux
// 	for (int jj=0; jj<ndim; jj++) flux[jj] = vx*q[jj]; //velocity flux
// 	flux[0] += press;
// 	flux[iE] = gamma1/(gamma1-1.)*press;
// 	flux[iE] += ekin;
// 	flux[iE] *= vx;

// 	return;
// }


void NoReconstruction::ReconstructConserved(Cell* cell){
	for (int cl=0; cl<ncells; cl++){
		for (int var=0; var<nvar;var++) cell[cl].qL[var] = cell[cl].Qcons[var];
		for (int var=0; var<nvar;var++) cell[cl].qR[var] = cell[cl].Qcons[var];

	}
	return;

}

void NoReconstruction::ReconstructPrimitive(Cell* cell){
	for (int cl=0; cl<ncells; cl++){
		cell[cl].Wprim[irho] = cell[cl].rho;
		cell[cl].WL[irho] = cell[cl].rho;
		cell[cl].WR[irho] = cell[cl].rho;
		cell[cl].Wprim[iE] = cell[cl].press;
		cell[cl].WL[iE] = cell[cl].press;
		cell[cl].WR[iE] = cell[cl].press;
		for (int jj=0; jj<ndim; jj++){
			cell[cl].Wprim[jj] = cell[cl].vel[jj];
			cell[cl].WL[jj] = cell[cl].vel[jj];
			cell[cl].WR[jj] = cell[cl].vel[jj];
		}
	}
	return;

}


void MinmodReconstruction::ReconstructConserved(Cell* cell){

	for (int cl=0; cl<ncells; cl++){
		for (int var = 0; var<nvar; var++){
			cell[cl].qL[var] = cell[cl].Qcons[var];
			cell[cl].qR[var] = cell[cl].Qcons[var];
		}

	}

	for (int cl=1; cl<ncells-1; cl++){
		Minmod(cell[cl-1].Qcons, cell[cl].Qcons, cell[cl+1].Qcons, cell[cl].qR);
		Minmod(cell[cl+1].Qcons, cell[cl].Qcons, cell[cl-1].Qcons, cell[cl].qL);

	}
	if(periodic_boundary == true){
		Minmod(cell[ncells-1].Qcons, cell[0].Qcons, cell[1].Qcons, cell[0].qR);
		Minmod(cell[1].Qcons, cell[0].Qcons, cell[ncells-1].Qcons, cell[0].qL);
		Minmod(cell[ncells-2].Qcons, cell[ncells-1].Qcons, cell[0].Qcons, cell[ncells-1].qR);
		Minmod(cell[0].Qcons, cell[ncells-1].Qcons, cell[ncells-2].Qcons, cell[ncells-1].qL);

	}

	// for (int cl=0; cl < ncells; cl++){
	// 	cout<<"cell: "<<cl<<endl;
	// 	cout<<"qL: ";
	// 	for (int var=0; var<nvar; var++)cout<<cell[cl].qL[var]<<"	";
	// 	cout<<endl;
	// 	cout<<"qR: ";
	// 	for (int var=0; var<nvar; var++)cout<<cell[cl].qR[var]<<"	";
	// 	cout<<endl;
	// 	cout<<"Qcons: ";
	// 	for (int var=0; var<nvar; var++)cout<<cell[cl].Qcons[var]<<"	";
	// 	cout<<endl;

	// }

	return;
}

void MinmodReconstruction::ReconstructPressure(Cell* cell){
	for (int cl=0; cl<ncells; cl++){
		cell[cl].PL = cell[cl].press;
		cell[cl].PR = cell[cl].press;
	}
	for (int cl=1; cl<ncells-1; cl++){
		Minmod(cell[cl-1].press, cell[cl].press, cell[cl+1].press, cell[cl].PL);
		Minmod(cell[cl+1].press, cell[cl].press, cell[cl-1].press, cell[cl].PR);

	}

}

void MinmodReconstruction::ReconstructPrimitive(Cell* cell){
	for (int cl=0; cl<ncells; cl++){
		cell[cl].Wprim[irho] = cell[cl].rho;
		cell[cl].WL[irho] = cell[cl].rho;
		cell[cl].WR[irho] = cell[cl].rho;
		cell[cl].Wprim[iE] = cell[cl].press;
		cell[cl].WL[iE] = cell[cl].press;
		cell[cl].WR[iE] = cell[cl].press;
		for (int jj=0; jj<ndim; jj++){
			cell[cl].Wprim[jj] = cell[cl].vel[jj];
			cell[cl].WL[jj] = cell[cl].vel[jj];
			cell[cl].WR[jj] = cell[cl].vel[jj];
		}
	}
	for (int cl=1; cl<ncells-1; cl++){
		Minmod(cell[cl-1].Wprim, cell[cl].Wprim, cell[cl+1].Wprim, cell[cl].WR);
		Minmod(cell[cl+1].Wprim, cell[cl].Wprim, cell[cl-1].Wprim, cell[cl].WL);

	}


}

void MinmodReconstruction::Minmod(double *ql, double *qm, double *qr, double *Qcons){
	double a, b;
	for (int var = 0; var<nvar; var++){

		a = qm[var] - ql[var];
		b = qr[var] - qm[var];
		// cout<<"a, b: "<<a<<" "<<b<<endl;
	
		if (a*b <= 0.){
			Qcons[var] = qm[var];
		} else if (a*b > 0.){
			// if (abs(a) > abs(b)){
			// 	Qcons[var] = 0.5*(3*qm[var] - qr[var]);
			// } else if (abs(a) <= abs(b)){
			// 	Qcons[var] = 0.5*(ql[var] + qm[var]);
			// }
			double r = b/a;
			double s = max(0., min(1.,r));
			Qcons[var] = qm[var] + 0.5*s*a;
		} else {
			cout<<"Error"<<endl;
		}
	
	}
}

void MinmodReconstruction::Minmod(double ql, double qm, double qr, double &Qcons){
	double a, b;

	a = qm - ql;
	b = qr - qm;
	// cout<<"a, b: "<<a<<" "<<b<<endl;

	if (a*b <= 0.){
		Qcons = qm;
	} else if (a*b > 0.){
		// if (abs(a) > abs(b)){
		// 	Qcons[var] = 0.5*(3*qm[var] - qr[var]);
		// } else if (abs(a) <= abs(b)){
		// 	Qcons[var] = 0.5*(ql[var] + qm[var]);
		// }
		double r = b/a;
		double s = max(0., min(1.,r));
		Qcons = qm + 0.5*s*a;
	} else {
		cout<<"Error"<<endl;
	}
	
}
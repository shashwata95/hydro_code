#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>

#include "initialize.h"
#include "all_constants.h"
#include "cell.h"


void::Initialize::WriteToFile(Cell* cell, int i, double time)
{
	ofstream writefile;
	string name;
	stringstream ss;
	ss<< setw(5)<<setfill('0')<<i;
	string s = ss.str();
	name = "./results/sod_recon/adsod_no_recon_" + s +".dat";
	writefile.open(name);
	writefile<<time<<endl;
	writefile<<ncells<<endl;
	for (int cl = 0; cl < ncells; cl++){
		for (int kk = 0; kk < ndim; kk++) writefile<<cell[cl].pos[kk]<<"	";
		for (int kk = 0; kk < ndim; kk++) writefile<<cell[cl].vel[kk]<<"	";
		writefile<<cell[cl].rho<<"	"<<cell[cl].press<<"	";
		writefile<<endl;
	}
	cout<<"file number: "<<i<<endl;
	writefile.close();

}


void Initialize::Adsod(Cell *cell, double rhoL, double rhoR, double PL, double PR)
{
	int border = ncells/2;
	double h = dx_global;
	double ekin;
	double min_len = -total_length/2.0;
	cell[0].boundary_cell = true;
	cell[ncells-1].boundary_cell = true;
	for (int i = 0; i < border; i++){ //left side of shock

		cell[i].volume = h;
		cell[i].rho = rhoL;
		for (int kk = 0; kk < ndim; kk++) cell[i].pos[kk] = 0.0;
		for (int kk = 0; kk < ndim; kk++) cell[i].vel[kk] = 0.0;
		for (int kk = 0; kk < ndim; kk++) cell[i].Bm[kk] = 0.0;
		cell[i].pos[0] = min_len + h*(i+0.5);
		cell[i].press = PL;
		cell[i].inten = cell[i].press/cell[i].rho/(gamma1-1);
		ekin = 0;
		for (int kk = 0; kk < ndim; kk++) ekin += cell[i].vel[kk]*cell[i].vel[kk];
		ekin = 0.5*ekin*cell[i].rho;


		cell[i].etot = ekin + cell[i].inten*cell[i].rho;

		for (int j=0; j<ndim; j++) cell[i].Wprim[j] = cell[i].vel[j]; //populating the primitive vector
		cell[i].Wprim[irho] = cell[i].rho;
		cell[i].Wprim[iE] = cell[i].press;
		cell[i].cs = sqrt(gamma1*cell[i].press/cell[i].rho);


	}

	for (int i = border; i < ncells; i++){ //right side of shock
		cell[i].volume = h;
		cell[i].rho = rhoR;
		for (int kk = 0; kk < ndim; kk++) cell[i].pos[kk] = 0.0;
		for (int kk = 0; kk < ndim; kk++) cell[i].vel[kk] = 0.0;
		for (int kk = 0; kk < ndim; kk++) cell[i].Bm[kk] = 0.0;
		cell[i].pos[0] = min_len + h*(i+0.5);
		// cell[i].vel[0] = 1.;
		cell[i].press = PR;
		cell[i].inten = cell[i].press/cell[i].rho/(gamma1-1);
		ekin = 0;
		for (int kk = 0; kk < ndim; kk++) ekin += cell[i].vel[kk]*cell[i].vel[kk];
		ekin = 0.5*ekin*cell[i].rho;

		cell[i].etot = ekin + cell[i].inten*cell[i].rho;

		for (int j=0; j<ndim; j++) cell[i].Wprim[j] = cell[i].vel[j]; //populating the primitive vector
		cell[i].Wprim[irho] = cell[i].rho;
		cell[i].Wprim[iE] = cell[i].press;
		cell[i].cs = sqrt(gamma1*cell[i].press/cell[i].rho);

	}

}

void Initialize::Sedov(Cell * cell, double energy, double radius)
{
	// double inner_frac  = 0.004; //two cells on each side, choose 0.1 for 
	double xmin_in = -radius;
	double xmax_in = radius;
	std::cout<<"inner part range: "<<xmin_in<<"	"<<xmax_in<<endl;
	for (int i=0; i<ncells; i++){
		double xpos = xmin + dx_global*(i+0.5);
		for (int kk=0; kk<ndim; kk++) cell[i].pos[kk] = 0.;
		for (int kk=0; kk<ndim; kk++) cell[i].vel[kk] = 0.;
		cell[i].pos[0] = xpos;
		cell[i].rho = 1.;
		if (xpos > xmin_in and xpos < xmax_in){
			cell[i].press = energy;
		}	else {
			cell[i].press = 1.;
		}
		cell[i].inten = cell[i].press/cell[i].rho/(gamma1-1);
		double ekin = 0;
		for (int kk = 0; kk < ndim; kk++) ekin += cell[i].vel[kk]*cell[i].vel[kk];
		ekin = 0.5*ekin*cell[i].rho;

		cell[i].etot = ekin + cell[i].inten*cell[i].rho;

		for (int j=0; j<ndim; j++) cell[i].Wprim[j] = cell[i].vel[j]; //populating the primitive vector
		cell[i].Wprim[irho] = cell[i].rho;
		cell[i].Wprim[iE] = cell[i].press;
		cell[i].cs = sqrt(gamma1*cell[i].press/cell[i].rho);

	}
	return;
}

void Initialize::Sinewave(Cell *cell, double lambda)
{
	for (int i=0; i<ncells; i++){
		double xpos = xmin + dx_global*(i+0.5);
		for (int kk=0; kk<ndim; kk++) cell[i].pos[kk] = 0.;
		for (int kk=0; kk<ndim; kk++) cell[i].vel[kk] = 1.;
		cell[i].pos[0] = xpos;
		cell[i].press = 1.;
		cell[i].rho = 1. + 0.1*sin(2.*3.1416*xpos/lambda);
		cell[i].inten = cell[i].press/cell[i].rho/(gamma1-1);

		double ekin = 0;
		for (int kk = 0; kk < ndim; kk++) ekin += cell[i].vel[kk]*cell[i].vel[kk];
		ekin = 0.5*ekin*cell[i].rho;

		cell[i].etot = ekin + cell[i].inten*cell[i].rho;

		for (int j=0; j<ndim; j++) cell[i].Wprim[j] = cell[i].vel[j]; //populating the primitive vector
		cell[i].Wprim[irho] = cell[i].rho;
		cell[i].Wprim[iE] = cell[i].press;
		cell[i].cs = sqrt(gamma1*cell[i].press/cell[i].rho);

	}

}
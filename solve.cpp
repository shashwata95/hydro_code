#include <iostream>
#include <cmath>
#include "solve.h"
#include "all_constants.h"

Solve::Solve()
{
	recon = reconstruction;
}

double Solve::GlobalFastWaveSpeed(Cell* cell)
{
	double fws = 0.;
	double tmp = 0.;
	for (int i = 0; i < ncells; i++){
		double cs2 = gamma1*cell[i].press/cell[i].rho;
		tmp = sqrt(cs2) + abs(cell[i].vel[0]);
		if (tmp > fws) fws = tmp;
	}
	// std::cout<<"fast wave speed: "<<fws<<endl;
	return fws;
}


void Solve::InitializeConserved(Cell* cell) //Converts primitive variables into conserved variables
{
	for (int i = 0; i < ncells; i++){
		cell[i].Qcons[irho] = cell[i].rho;
		cell[i].Qcons[iE] = cell[i].etot;
		for (int kk = 0; kk < ndim; kk++) cell[i].Qcons[kk] = cell[i].rho*cell[i].vel[kk];
		for (int var =0; var < nvar; var++) cell[i].Qcons0[var] = cell[i].Qcons[var];

		for (int var =0; var < nvar; var++) cell[i].dQ[var] = 0.;
	}

}

void Solve::CellFromQcons(Cell* cell, double dt) //Converts conserved variables into primitive variables
{
	for (int i = 0; i < ncells; i++){
		cell[i].rho = cell[i].Qcons[irho];
		cell[i].etot = cell[i].Qcons[iE];
		for (int j = 0; j < ndim; j++){
			cell[i].vel[j] = cell[i].Qcons[j]/cell[i].Qcons[irho];
		}
		double ekin = 0;
		for (int j = 0; j < ndim; j++) {
			ekin += 0.5*cell[i].rho*cell[i].vel[j]*cell[i].vel[j];
		}
		cell[i].inten = (cell[i].etot - ekin)/cell[i].rho;
		cell[i].press = (gamma1-1.)*cell[i].rho*cell[i].inten;
	}
}

void Solve::InitializeFlux(double* flux){
	for (int jj=0; jj<nvar; jj++){
		flux[jj] = 0.;
	}
	return;
}

double Solve::FastWaveSpeed(Cell cL, Cell cR){ //Calculates local lambda max
	double csL = gamma1*cL.WR[iE]/cL.WR[irho];
	csL  = sqrt(csL) + abs(cL.WR[0]);
	double csR = gamma1*cR.WL[iE]/cR.WL[irho];
	csR  = sqrt(csR) + abs(cR.WL[0]);
	return max(csL, csR);

}


double Solve::FastWaveSpeed(double * qL, double *qR){ //same from conserved quantities
	double pL, pR;
	double ekinL=0., ekinR=0.;
	double csL, csR;
	double vL, vR;
	for (int kk=0; kk<ndim; kk++) ekinL += 0.5*qL[kk]*qL[kk]/qL[irho];
	for (int kk=0; kk<ndim; kk++) ekinR += 0.5*qR[kk]*qR[kk]/qR[irho];
	pL = (gamma1-1.)*(qL[iE] - ekinL);
	pR = (gamma1-1.)*(qR[iE] - ekinR);
	csL = sqrt(gamma1*pL/qL[irho]);
	csR = sqrt(gamma1*pR/qR[irho]);
	vL = sqrt(2.*ekinL/qL[irho]);
	vR = sqrt(2.*ekinR/qR[irho]);
	return max(csL+vL, csR+vR);



}

void Solve::UpdateDQ(Cell &cL, Cell &cR, double *flux, double dt){
	for (int var=0; var<nvar; var++){
		cL.dQ[var] -= dt/dx_global*flux[var]; //flux subtracted from left cell and added to right cell
		cR.dQ[var] += dt/dx_global*flux[var];
	}
	return;
}


void Solve::BoundaryCheck(Cell* cell, double * flux, double dt){ //closed boundary conditions, need modification for periodic

	if (periodic_boundary == false){
		for (int var=0; var<nvar; var++) {
			cell[0].Qcons[var] = cell[1].Qcons[var];
			cell[ncells-1].Qcons[var] = cell[ncells-2].Qcons[var];
		}
	} else if (periodic_boundary == true){
		RiemannLF riemannLF;
		double flux[nvar];
		double vmax = FastWaveSpeed(cell[ncells-1], cell[0]); 	//compute lambda max
		riemannLF.ComputeFlux(cell[ncells-1],cell[0],flux,vmax); //compute fluxes between a pair of cells: can be improved by
																//using different Riemann solver or reconstructed flux
		UpdateDQ(cell[ncells-1], cell[0], flux, dt);
		for (int var=0; var<nvar; var++){
			cell[0].Qcons[var] += cell[0].dQ[var];
			cell[ncells-1].Qcons[var] += cell[ncells-1].dQ[var];		
			cell[ncells-1].dQ[var] = 0.;
			cell[0].dQ[var] = 0.;
		}
	
	}
	return;
}

void Solve::UpdateQcons(Cell * cell){
	for (int cl=0; cl<ncells; cl++){

		for (int var=0; var<nvar; var++) cell[cl].Qcons[var] += cell[cl].dQ[var];
		for (int var=0; var<nvar; var++) cell[cl].dQ[var] = 0.;
		// for (int var=0; var<nvar; var++) cell[cl].Qcons[var] = cell[cl].Qcons0[var];

	}
	return;
}

void Solve::UpdateCellValue(Cell* cell, double dt)
{
	double flux[nvar];
	RiemannLF riemannLF;
	if (recon == true){
		MinmodReconstruction rec;
		rec.ReconstructConserved(cell);
		rec.ReconstructPrimitive(cell);
		// rec.ReconstructPressure(cell);
	} else{
		NoReconstruction rec;
		rec.ReconstructPrimitive(cell);
		rec.ReconstructConserved(cell);
	}

	// for (int cl=0; cl<ncells; cl++){
	// 	cout<<cl<<endl;
	// 	cout<<"WL: ";
	// 	for (int var=0; var<nvar;var++)cout<<cell[cl].WL[var]<<"	";
	// 	cout<<endl;
	// 	cout<<"WR: ";
	// 	for (int var=0; var<nvar;var++)cout<<cell[cl].WR[var]<<"	";
	// 	cout<<endl;
	// 	cout<<"Wprim: ";
	// 	for (int var=0; var<nvar;var++)cout<<cell[cl].Wprim[var]<<"	";
	// 	cout<<endl;

	// }


	for (int i = 0; i<ncells-1; i++){ 						//loop over all cells in the row

		InitializeFlux(flux); 								//set all flux values to 0
		// double vmax = FastWaveSpeed(cell[i], cell[i+1]); 	//compute lambda max
		// riemannLF.ComputeFlux(cell[i],cell[i+1],flux,vmax); //compute fluxes between a pair of cells: can be improved by

		double vmax = FastWaveSpeed(cell[i], cell[i+1]); 	//compute lambda max
		// std::cout<<"vmax: "<<vmax<<endl;
		riemannLF.ComputeFlux(cell[i],cell[i+1],flux,vmax); //compute fluxes between a pair of cells: can be improved by
															//using different Riemann solver or reconstructed flux
		UpdateDQ(cell[i], cell[i+1], flux, dt);

	}
	UpdateQcons(cell);										//Update conserved variable array
	BoundaryCheck(cell, flux, dt);							//implements closed boundary conditions
	CellFromQcons(cell, dt);								//updates primitive variables
	return;
}

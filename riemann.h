#ifndef RIEMANN_H
#define RIEMANN_H
#include <iostream>

#include "cell.h"
#include "all_constants.h"

class Riemann
{
public:
	virtual void ComputeFlux(Cell cL, Cell cR, double *flux, double vmax){
		return;
	}
	virtual void ComputeFlux(double*, double*, double *flux, double vmax){
		return;
	}
	

};
class RiemannLF: public Riemann
{
public:
	void ComputeFlux(Cell cL, Cell cR, double *flux, double vmax);
	void ComputeFlux(double*, double*, double *flux, double vmax);
	void CellCentredFlux(Cell, double*);
	void CellCentredFlux(double*, double*);

	
};

class Reconstruction
{
public:
	virtual void ReconstructConserved(Cell*){
		return;
	}	
};


class NoReconstruction: public Reconstruction
{
public:
	void ReconstructPrimitive(Cell*);
	void ReconstructConserved(Cell*);
	
};

class MinmodReconstruction: public Reconstruction
{
public:
	void ReconstructConserved(Cell*);
	void ReconstructPrimitive(Cell*);
	void Minmod(double *, double*, double*, double*);
	void Minmod(double, double, double, double&);
	void ReconstructPressure(Cell*);
};

#endif
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "all_constants.h"
#include "cell.h"
#include "initialize.h"
#include "solve.h"

int main(int argc, char const *argv[])
{
	//Initialization
	Cell* cell = (Cell*)calloc(ncells,sizeof(Cell)); 	//Initialize cells
	Initialize init;
	Solve solve;


	double time = 0, dt;
	int counter = 1;
	// init.Sedov(cell, 1000., .01); //P_centre, radius_centre
	init.Adsod(cell, 1., 0.125, 1., 0.1); //rhoL, rhoR, PL, PR
	// init.Sinewave(cell, 1.);
	init.WriteToFile(cell,0, 0.);
	solve.InitializeConserved(cell);

	for (int steps=0; steps<big_number; steps++){
		if (time > tfinal) break;

		double vfast = solve.GlobalFastWaveSpeed(cell); //calculate wave speed
		double dt = solve.GlobalTimestep(vfast); 		//calculate timestep
		solve.UpdateCellValue(cell, dt); 				//Compute fluxes and add to the new values, update everything

		time += dt;	
		if (time >= dt_snap*counter){
		// if (time >= 0){
			std::cout<<"time: "<<time<<" dt: " << dt<<endl;
			init.WriteToFile(cell,counter,time);
			counter++;

		}

	}
	// init.WriteToFile(cell,1,time);


	free(cell);
	return 0;
}

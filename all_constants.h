#ifndef ALLCONSTANTS_H
#define ALLCONSTANTS_H
#include <iostream>
#include <string>

static const int ncells = 1000;
static const double tfinal = .2;
static const double dt_snap = .1;
static const double gamma1 = 1.4;
static const double xmin = -.5; //a: domain left edge
static const double xmax = .5; //b: domain right edge
static const double cfl = 0.1;
static const bool reconstruction = false;
static const bool periodic_boundary = false;

static const int ndim = 1;
static const int nvar = ndim + 2; //index order: velocity->0,1,2 density->3, energy->4
static const int irho = ndim;
static const int iE = ndim + 1;
static const std::string ic = "adsod";
static const double total_length = xmax - xmin;
static const double dx_global = total_length/ncells;
static const int big_number = 999999;

#endif

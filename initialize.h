#ifndef INITIALIZE_H
#define INITIALIZE_H
#include <iostream>
#include <string>

#include "all_constants.h"
#include "cell.h"

class Cell;

class Initialize
{
public:

	// ~Initialize();
	void Adsod(Cell *, double, double, double, double);
	void Sedov(Cell *, double, double);
	void Sinewave(Cell*, double);
	void WriteToFile(Cell *, int i, double time);
	
};

#endif

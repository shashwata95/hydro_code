import numpy as np 
import matplotlib
import matplotlib.pyplot as plt

def compute_error(nu, rec =False):
	if (rec == False):
		name = 'sine_nu_' + str(nu) + '_00020.dat' 
	elif (rec == True):
		name = 'sine_rec_nu_' + str(nu) + '_00020.dat' 
	x = np.loadtxt(name, skiprows = 2)
	xpos = x[:,0]
	den = x[:,2]
	model = 1. + 0.1*np.sin(2.*np.pi*xpos)
	err = np.absolute(model - den)
	number = len(xpos)
	return np.sum(err)/number

def eoc(x, y):
	eoc = []
	for i in range(1,len(x)):
		value = (np.log(y[i-1]) - np.log(y[i]))/(np.log(x[i]) - np.log(x[i-1]))
		eoc.append(value)
	return eoc

x = []
y1 = []
y2 = []
for i in range(5,10):
	x.append(10.*np.power(2,i))
	y1.append(compute_error(i))
	y2.append(compute_error(i, rec = True))

print(x)
# print(y1)

eoc1 = eoc(x,y1)
eoc2 = eoc(x,y2)
print(eoc1)
print(eoc2)
fig, ax = plt.subplots(nrows=1, ncols=1)
ax.plot(x, y1, 'bo', label='no reconstruction')
ax.plot(x, y2, 'ro', label='minmod')
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel('No of cells')
ax.set_ylabel('eoc')
plt.legend()
plt.show()
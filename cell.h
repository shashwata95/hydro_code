#ifndef CELL_H
#define CELL_H

#include <iostream>

#include "all_constants.h"
using namespace std;

class Cell
{
public:

	// ~Cell();
	int id[ndim];
	double pos[ndim];
	double vel[ndim];
	double rho;
	double etot; //Energy per unit volume
	double Bm[ndim];
	double volume;

	double Qcons[nvar];
	double Qcons0[nvar];
	double dQ[nvar];
	double Wprim[nvar];
	double qL[nvar];
	double qR[nvar];
	double PL;
	double PR;
	double WL[nvar];
	double WR[nvar];

	double inten; //Energy per unit mass
	double press;
	double dx;
	double vface[ndim];
	double cs;
	double FluxCen[nvar];
	bool boundary_cell = false;
	// double PressFromEtot() {

	// }


	
};
#endif

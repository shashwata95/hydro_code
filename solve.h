#ifndef SOLVE_H
#define SOLVE_H

#include <iostream>

#include "cell.h"
#include "all_constants.h"
#include "riemann.h"

class Cell;
class Riemann;

class Solve
{
public:
	bool recon;
	Solve();

	double FastWaveSpeed(Cell cL, Cell cR);
	double FastWaveSpeed(double *, double *);
	double GlobalFastWaveSpeed(Cell*);
	void InitializeConserved(Cell*);
	void CellFromQcons(Cell*, double dt);
	double GlobalTimestep(double vfast){
		// std::cout<<"timestep: "<<cfl*dx_global/vfast<<endl;
		return cfl*dx_global/vfast;
	};
	void InitializeFlux(double*);
	void UpdateCellValue(Cell*, double);
	void UpdateDQ(Cell&, Cell&, double*, double);
	void BoundaryCheck(Cell*, double *, double);
	void UpdateQcons(Cell*);
};

#endif